// guest.ts

export interface Guest {
  _id: string;
  Firstname: string;
  Lastname: string;
  createdAt: string;
  Notes: string;
}

