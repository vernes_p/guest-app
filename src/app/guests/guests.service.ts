import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Guest } from './guest';

@Injectable({
  providedIn: 'root'
})
export class GuestsService {

  private apiEndpoint = `${environment.apiEndpoint}/guests?_limit=5`;

  constructor(private http: HttpClient) {
  }

  public getGuests(): Observable<Guest[]> {
    return this.http.get<Guest[]>(this.apiEndpoint);
  }
}
