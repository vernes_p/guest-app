// guest.module.ts

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuestsComponent } from './guests.component';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { RouterModule, Routes } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { GuestsService } from './guests.service';
import { AuthInterceptor } from '../auth-interceptor';
import { StoreModule } from '@ngrx/store';
import * as fromGuest from './+store/guest.reducer';
import { EffectsModule } from '@ngrx/effects';
import { GuestEffects } from './+store/guest.effects';

const routes: Routes = [
  {
    path: '',
    component: GuestsComponent
  }
];

@NgModule({
  declarations: [GuestsComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MatCardModule,
    MatListModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature(fromGuest.guestFeatureKey, fromGuest.reducer),
    EffectsModule.forFeature([GuestEffects])
  ],
  providers: [
    GuestsService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ]
})
export class GuestsModule {
}
