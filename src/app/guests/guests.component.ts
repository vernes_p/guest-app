import { Component, OnInit } from '@angular/core';
import { Guest } from './guest';
import { State } from './+store/guest.reducer';
import { Store } from '@ngrx/store';
import { selectGuests } from './+store/guest.selectors';
import { Observable } from 'rxjs';
import { loadGuests } from './+store/guest.actions';

@Component({
  selector: 'app-guests',
  templateUrl: './guests.component.html',
  styleUrls: ['./guests.component.scss']
})
export class GuestsComponent implements OnInit {

  public guests: Observable<Guest[]> = this.store.select(selectGuests);

  constructor(private store: Store<State>) {
  }

  ngOnInit(): void {
    this.store.dispatch(loadGuests());
  }

}
