// guest.selectors.ts

import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromGuest from './guest.reducer';

export const selectGuestState = createFeatureSelector<fromGuest.State>(
  fromGuest.guestFeatureKey
);

export const selectGuests = createSelector(
  selectGuestState,
  state => state.guests
);
