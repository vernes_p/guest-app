// guest.actions.ts

import { createAction, props } from '@ngrx/store';
import { Guest } from '../guest';

export const loadGuests = createAction(
  '[Guest] Load Guests'
);

export const loadGuestsSuccess = createAction(
  '[Guest] Load Guests Success',
  props<{ guests: Guest[] }>()
);

export const loadGuestsFailure = createAction(
  '[Guest] Load Guests Failure',
  props<{ error: any }>()
);
