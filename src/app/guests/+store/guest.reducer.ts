// guest.reducer.ts

import { Action, createReducer, on } from '@ngrx/store';
import * as GuestActions from './guest.actions';
import { Guest } from '../guest';

export const guestFeatureKey = 'guest';

// this is our state
export interface State {
  guests: Guest[];
  error: any;
}

// we need to provide an initial state
export const initialState: State = {
  guests: null,
  error: null
};

// reducer functions to manipulate the state
const guestReducer = createReducer(
  initialState,

  on(GuestActions.loadGuestsSuccess, (state, action) => {
    // state is immutable => we always create a new state
    return {...state, guests: action.guests, error: null};
  }),
  on(GuestActions.loadGuestsFailure, (state, action) => {
    // state is immutable => we always create a new state
    return {...state, guests: null, error: action.error};
  }),
);

export function reducer(state: State | undefined, action: Action) {
  return guestReducer(state, action);
}
