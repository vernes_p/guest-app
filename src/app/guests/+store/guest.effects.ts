// guest.reducer.ts

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import * as GuestActions from './guest.actions';
import { GuestsService } from '../guests.service';
import { loadGuestsFailure, loadGuestsSuccess } from './guest.actions';


@Injectable()
export class GuestEffects {

  loadGuests$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(GuestActions.loadGuests),
      switchMap(() =>
        this.guestsService.getGuests().pipe(
          map(guests => loadGuestsSuccess({guests})),
          catchError(
            error => of(loadGuestsFailure({error}))
          )
        ))
    );
  });

  constructor(private actions$: Actions,
              private guestsService: GuestsService) {
  }

}
