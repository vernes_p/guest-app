import * as fromGuest from './guest.actions';

describe('loadGuests', () => {
  it('should return an action', () => {
    expect(fromGuest.loadGuests().type).toBe('[Guest] Load Guests');
  });
});
