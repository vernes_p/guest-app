import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any> | any> {

    if (request.url.includes('auth/local')) {
      return next.handle(request);
    }

    const headers = {
      // tslint:disable-next-line:max-line-length
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlM2M3OGYyMjZhYjNhYjQ0MTU5MWY3ZiIsImlhdCI6MTU4NDE3OTgyOSwiZXhwIjoxNTg2NzcxODI5fQ.VVeWaccELdgYjvJwXjoyA00Xs-TfZJvUZd9t8Dxvp6g`,
      'Content-Type': 'application/json'
    };

    request = request.clone({
      setHeaders: headers
    });
    return next.handle(request);
  }
}
