import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyDashboardComponent } from './my-dashboard/my-dashboard.component';


const routes: Routes = [
  {
    path: 'home',
    component: MyDashboardComponent
  },
  {
    path: 'fullpage',
  },
  {
    path: 'guests',
    loadChildren: () => import('./guests/guests.module').then(m => m.GuestsModule)
  },
  {
    path: 'guests',
    loadChildren: () => import('./guests/guests.module').then(m => m.GuestsModule),
    outlet: 'left'
  },
  {
    path: 'guests',
    loadChildren: () => import('./guests/guests.module').then(m => m.GuestsModule),
    outlet: 'right'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
